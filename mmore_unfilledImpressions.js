// only run if global var allows
if (mmore_RunUnfilledImpressions) mmmore_startAdChecker();  

function mmmore_startAdChecker() {
    if (window.googletag && googletag.apiReady) {
        // try checking for unfilled imps via standard google events
        googletag.pubads().addEventListener('slotRenderEnded', function(event) {
            console.log("slotRenderEnded callback:");

            // if empty, find ad size and then replace with backfill
            if (event.isEmpty) {
                var divSlot = document.getElementById(event.slot.getSlotElementId());
                var size = event.size.toString().replace(",","x");
                mmmore_replaceAd(event.slot, size);
            }
        });
        
        // googletag many be blocked or ads may not be done loading, so pause and check again
        window.addEventListener("load", function(event) {
            console.log("load callback:");
            
            // make sure we leave enough time for ads to fully load
            setTimeout(mmmore_checkAdsAfterCompleteLoad, mmore_LoadCompleteTimeout);    
        });
    } else {
        setTimeout(mmmore_startAdChecker, 100);
    }
}

// run this after we've waited for all ads to load on page
function mmmore_checkAdsAfterCompleteLoad() {
    // look through slots to see if corresponding divs are on the page
    var slotsOnPage = googletag.pubads().getSlots();
    for (var i = 0; i < slotsOnPage.length; i++) {
        // ok this is a slot that should have rendered
        var divSlot = document.getElementById(slotsOnPage[i].getSlotElementId());
        if (divSlot !== null) {
            // if slot is empty, replace with backfill using one of the pred-defined sizes
            if (mmmore_isSlotEmpty(divSlot)) {
                mmmore_replaceAd(slotsOnPage[i], "1x1");
            }
        }
    }
}

// try to see if an ad didn't fill by seeing if known google ad elements (divs, iframes) are there
function mmmore_isSlotEmpty(divSlot) {
    // for testing purposes
    if (window.location.href.includes("mmoreForceReload=true")) return true;
    
    if (divSlot.firstChild === null) { console.log("firstChild null");return true; }
    if (divSlot.firstChild.firstChild === null) { console.log("firstChild.firstChild null");return true; }
    if (divSlot.firstChild.firstChild.contentWindow === null) { console.log("contentWindow null");return true; }
    
    // we shouldn't be able to access contents of iframes from other domains
    try {
        if (divSlot.firstChild.firstChild.contentWindow.document === null) { console.log("document null");return true; }
        if (divSlot.firstChild.firstChild.contentWindow.document.body === null) { console.log("document.body null");return true; }
        if (divSlot.firstChild.firstChild.contentWindow.document.body.children.length === 0) { console.log("document.body.children null");return true; }
    } catch(err) {
        if (err.name !== "SecurityError") { console.log(err.name);return true; }
        if (!err.message.includes("cross-origin")) { console.log(err.message);return true; }
    }
    
    // all current checks pass, ad *should* not be empty
    return false;
}

// given a google slot and a "NxM" size, find and replace ad from configured ad network
function mmmore_replaceAd(slot, size) {
    // lookup matching replacement based on ad size
    var ad = mmore_getReplacementAd(size);
    
    // sometimes google mistakenly says its a 1x1 ad when its not
    // if there is no matching replacement ad, try the predefined sizes
    var sizes = slot.getSizes();
    var i = 0;
    while ((ad === "") && (i < sizes.length)) {
        if (typeof sizes[i] === 'object') {
            size = sizes[i].l + 'x' + sizes[i].j;
        } else {
            size = sizes[i];
        }
        ad = mmore_getReplacementAd(size);
        i++;
    }
    
    // found a replacement ad, insert into page via iframe
    if (ad !== "") {
        var iframe = document.createElement('iframe');
        iframe.frameBorder = 0;
        iframe.scrolling = "no";
        iframe.width = size.split("x")[0];
        iframe.height = size.split("x")[1];
        
        // safest to blow away google slot div and put new ad in its place
        var divSlot = document.getElementById(slot.getSlotElementId());
        var sibling = divSlot.nextSibling;
        var parent = divSlot.parentNode;
        parent.removeChild(divSlot);
        if (sibling !== null) {
            parent.insertBefore(iframe, sibling)
        } else {
            parent.appendChild(iframe);
        }
        
        var content = "<!DOCTYPE html><body style='margin:0px;'><script type=\"text/javascript\" src=" + ad + "></script></body></html>";
        
        // for testing purposes
        if (window.location.href.includes("mmoreTestAd=true")) {
            content = "<!DOCTYPE html><body style='margin:0px;background-color:yellow;'>put hard-coded ad here</body></html>";
        }

        iframe.contentWindow.contents = content;
        iframe.src = 'javascript:window["contents"]';
        console.log("unfilled ad: replaced:" + size);
    } else {
        // no replacement ad found, do nothing
        console.log("unfilled ad: no matching size:" + size);
    }
}
