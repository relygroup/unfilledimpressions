# Introduction
Javascript to detect unfilled DFP ad impressions and backfill.

# Operation
The script does this by:

1. checking standard Google events
2. looking for expected elements of standard google DFP ads

The backfill networks are currently supported:

1. AdNXS

BONUS: This script also works against the AdBlock ad-blocker although the backfill ad must be static (cannot use ads loaded via js)

# Installation
Download the following script files and add it to your website

1. mmore_unfilledImpressions.js
2. mmore_unfilledAdNXS.js

Reference the javascript files (see Usage)

Set the javascript config variables (see Configuration)

# Usage
Look for a location *after* the Google DFP googletag object services are enabled: 
```
googletag.enableServices();
```
Reference the files like this:
```
<!-- START Unfilled Impressions -->
<script language="javascript">
var mmore_RunUnfilledImpressions = true;
var mmore_LoadCompleteTimeout = 1000;
</script>

<!-- include one ad network you want to use as backfill -->
<script type="text/javascript" src="js/mmore_unfilledAdNXS.js"></script>
<!--<script type="text/javascript" src="js/mmore_unfilledXYZNetwork.js"></script>-->

<script type="text/javascript" src="js/mmore_unfilledImpressions.js"></script>
<!-- END Unfilled Impressions -->
```

# Configuration
Before using this script, check to see if your DFP ad configuration is setup properly, [here](https://www.monetizemore.com/blog/unfilled-ad-impressions-troubleshooting/) is a good article that can help you do this.

Note the following config variables:

1. mmore_RunUnfilledImpressions: boolean : indicate whether you want to run the script
2. mmore_LoadCompleteTimeout: (ms) : timeout before the script starts to check for ads on page

# FAQ
Q. How do I backfill with another ad network?

A. Copy the file *mmore_unfilledAdNXS.js*, replace the function *mmore_getReplacementAd(size)*, and reference your hew file instead of *mmore_unfilledAdNXS.js*
